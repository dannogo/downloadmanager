package com.zultys.justpolygone

import android.app.DownloadManager
import android.app.DownloadManager.Request.NETWORK_MOBILE
import android.app.DownloadManager.Request.NETWORK_WIFI
import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.format.Formatter
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import java.io.File
import java.util.*
import java.util.concurrent.ThreadLocalRandom

fun Any.loge(text: String) = Log.e(this.javaClass.canonicalName, text)
fun Any.logd(text: String) = Log.d(this.javaClass.canonicalName, text)
fun Any.logi(text: String) = Log.i(this.javaClass.canonicalName, text)
fun Any.logw(text: String) = Log.w(this.javaClass.canonicalName, text)
fun getFormattedFileSize(context: Context, size: Long?): String = Formatter.formatShortFileSize(context, size ?: 0L)

class MainActivity : AppCompatActivity() {

    companion object {
        const val DIRECTORY_FILETRANSFER = "Filetransfer"
    }

    private val random = ThreadLocalRandom.current()
    private lateinit var downloadManager: DownloadManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        clearFiletransferFolder()
    }

    private fun clearFiletransferFolder() {
        val destinationFolder = getExternalFilesDir(DIRECTORY_FILETRANSFER)
        if (destinationFolder != null
            && destinationFolder.exists()
            && destinationFolder.isDirectory){

            for (file in destinationFolder.listFiles()) {
                if (!file.isDirectory) file.delete()
            }
        }
    }

    private fun enqueueFileInDownloadManager() {
//        val uriString = "https://${userSettings.serverAddressNewApi}/?session=${userSettings.sessionUser.uuid}&command=cli_download_file&messageId=${messageEvent.id}"
//        val uriString = "https://10.10.30.98/newapi/?session=564b025f-6a70-4e7a-9f49-a3aeb3ac62c7&command=cli_download_file&messageId=-1109036803277176821"
        val uriString = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
        val uri = Uri.parse(uriString)
        val randomInt = random.nextInt(0, 9999)
        val destinationFolder = getExternalFilesDir(DIRECTORY_FILETRANSFER)
            ?: throw IllegalStateException("Seems external storage is not mounted")
        if (!destinationFolder.exists()) destinationFolder.mkdirs()
        val destinationFile = File(destinationFolder, "file_$randomInt")
        val destinationUri = Uri.fromFile(destinationFile);
        val request = DownloadManager.Request(uri)
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
            .setAllowedNetworkTypes(NETWORK_WIFI or NETWORK_MOBILE)
            .setAllowedOverRoaming(false)
            .setTitle("Demo ${UUID.randomUUID().toString().substring(0, 2)}")
            .setDescription("Standard description")
            .setDestinationUri(destinationUri)
        val lastDownload = downloadManager.enqueue(request)

        logd("DMT request id ${lastDownload}")
    }

    private fun listOutFiletransferFiles() {
        val destinationFolder = getExternalFilesDir(DIRECTORY_FILETRANSFER)
            ?: throw IllegalStateException("Seems external storage is not mounted")
        val files: Array<File> = destinationFolder.listFiles()
            ?: throw IllegalStateException("List of files is null")
        if (files.isEmpty()) {
            logd("DMT there are no files")
        }
        for (file: File in files) {
            logd("DMT isFile: ${file.isFile} , title: ${file.name}, size: ${getFormattedFileSize(this, file.length())}")
        }
    }

    fun download(view: View) {
        listOutFiletransferFiles()
        enqueueFileInDownloadManager()
    }
}